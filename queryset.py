from django.db import models
from django.db.models import F
from django.db.models import Q

from nested_intervals.exceptions import InvalidNodeError
from nested_intervals.matrix import get_child_matrix
from nested_intervals.matrix import get_ancestors_matrix
from nested_intervals.matrix import get_root_matrix
from nested_intervals.matrix import INVISIBLE_ROOT_MATRIX
from nested_intervals.matrix import Matrix
from nested_intervals.exceptions import NoChildrenError
from nested_intervals.validation import validate_node

import functools
import operator

def build_nested_intervals_query_kwargs(cls, a11, a12, a21, a22):
    name11, name12, name21, name22, parent_name = cls._nested_intervals_field_names
    return {
        name11: abs(a11),
        name12: abs(a12),
        name21: abs(a21),
        name22: abs(a22),
    }

def get_queryset_or_empty(model_class, query):
    if query:
        return model_class.objects.filter(query)
    return model_class.objects.none()

######################
# INSTANCE FUNCTIONS #
######################

def get_matrix(instance):
    return Matrix(*(
        getattr(instance, field_name) * (1 if (i % 2 == 0) else -1)
        for i, field_name in enumerate(instance._nested_intervals_field_names[0:-1]))
    )

def get_abs_matrix(instance):
    return Matrix(*tuple(abs(num) for num in get_matrix(instance)))

def get_nth(instance):
    n11, n12, n21, n22, parent_name = instance._nested_intervals_field_names
    return int(getattr(instance, n11) / getattr(instance, n12))

def set_matrix(instance, matrix):
    for field_name, num in zip(instance._nested_intervals_field_names, matrix):
        setattr(instance, field_name, abs(num))

def set_parent(instance, parent):
    parent_name = instance._nested_intervals_field_names[-1]
    setattr(instance, parent_name, parent)

def set_as_child_of(instance, parent):
    if parent:
        parent_matrix = get_matrix(parent)
    else:
        parent_matrix = INVISIBLE_ROOT_MATRIX
    child_matrix = get_child_matrix(
        parent_matrix,
        last_child_nth_of(type(instance).objects, parent_matrix) + 1)

    try:
        validate_node(instance)
    except InvalidNodeError:
        # A new model instance is being created,
        # so a setting the matrix is enough.
        set_matrix(instance, child_matrix)
        set_parent(instance, parent)
        return (instance,)

    # instance is an existing model instance which may have
    # descendants, so the instance and its descendants'
    # matrices must be updated, using the reroot function.
    return reroot(instance, parent, child_matrix)

def save_as_child_of(instance, parent, *args, **kwargs):
    nodes = set_as_child_of(instance, parent)
    for node in nodes:
        node.save(*args, **kwargs)
    return nodes

def set_as_root(instance):
    return set_as_child_of(instance, None)

def save_as_root(instance, *args, **kwargs):
    set_as_root(instance)
    instance.save(*args, **kwargs)
    return instance

def get_descendants_of_matrix(model_class, matrix):
    name11, name12, name21, name22, parent_name = model_class._nested_intervals_field_names
    a11, a12, a21, a22 = map(abs, matrix)

    s1 = a11 - a12 # 's' stands for sibling
    s2 = a21 - a22

    return model_class.objects.extra(
        where=[
            "({} * %s) >= (%s * {})".format(name11, name21),
            "({} * %s) <= (%s * {})".format(name12, name22)
        ],
        params=[s2, s1, a21, a11])

def get_descendants(instance):
    return get_descendants_of_matrix(
        instance.__class__,
        get_abs_matrix(instance))

def get_ancestors_query_of_matrix(model_class, matrix):
    real_matrix = Matrix(
        matrix[0],
        -abs(matrix[1]),
        matrix[2],
        -abs(matrix[3]))

    return reduce(
        lambda a, b: a | Q(
            **build_nested_intervals_query_kwargs(model_class, *b)),
        get_ancestors_matrix(real_matrix),
        Q())

def get_ancestors_query(instance):
    return get_ancestors_query_of_matrix(
        instance.__class__,
        get_matrix(instance))

def get_ancestors(instance):
    return get_queryset_or_empty(
        instance.__class__,
        get_ancestors_query(instance))

def get_family_line(instance):
    """
    This includes instance, ancestors, and descendants.
    This EXCLUDES siblings and cousins.
    """
    return get_ancestors(instance) | get_descendants(instance) | instance.__class__.objects.filter(pk=instance.pk)

def get_parent(instance):
    name11, name12, name21, name22, parent_name = instance._nested_intervals_field_names
    return instance.__class__.objects.get(**{
        name11: getattr(instance, name12),
        name21: getattr(instance, name22)
    })

def get_root(instance):
    return instance.__class__.objects.get(
        **build_nested_intervals_query_kwargs(
            instance.__class__,
            *get_root_matrix(get_matrix(instance))))

######################
# QUERYSET FUNCTIONS #
######################

def children_of_matrix(queryset, matrix):
    name11, name12, name21, name22 = queryset.model._nested_intervals_field_names[0:4]
    parent_value11, parent_value12, parent_value21, parent_value22 = matrix

    return queryset.filter(**{
        name12: parent_value11,
        name22: parent_value21
    })

def get_children(parent, queryset=None):
    validate_node(parent)
    if queryset is None:
        queryset = parent.__class__.objects

    name11, name12, name21, name22, parent_name = parent._nested_intervals_field_names
    parent_value11, parent_value12, parent_value21, parent_value22 = get_abs_matrix(parent)

    return queryset.filter(**{
        name12: parent_value11,
        name22: parent_value21
    })

def last_child_of_matrix(queryset, parent_matrix):
    name11, name12, name21, name22, parent_name = queryset.model._nested_intervals_field_names
    v11, v12, v21, v22 = (abs(v) for v in parent_matrix)

    try:
        return queryset.filter(**{
            name12: v11,
            name22: v21
        }).order_by((F(name11) / F(name12)).desc())[0]
    except IndexError:
        raise NoChildrenError()

def last_child_of(parent):
    validate_node(parent)
    name11, name12, name21, name22, parent_name = parent._nested_intervals_field_names
    try:
        return get_children(parent).order_by((F(name11) / F(name12)).desc())[0]
    except IndexError:
        raise NoChildrenError()

def last_child_nth_of(queryset, parent_matrix):
    try:
        last_child = last_child_of_matrix(queryset, parent_matrix)
        return get_nth(last_child)
    except NoChildrenError:
        return 0

def reroot(node, parent, child_matrix):
    validate_node(node)
    validate_node(parent)
    parent_matrix = get_matrix(parent)
    children = get_children(node)

    set_matrix(node, child_matrix)
    set_parent(node, parent)

    descendants = functools.reduce(
        operator.add,
        (
            reroot(
                child,
                node,
                get_child_matrix(child_matrix, i+1))
            for i, child in enumerate(children)
        ),
        (),
    )
    return (node,) + tuple(children) + descendants
